package com.zenithwebfoundry.blog.fixtures

import com.zenithwebfoundry.blog.model.Article

class ArticleFixtures {

    static def ETHEL_ARTICLE = new Article(
            "10343816-c884-4c79-9ab4-6dd4c740c52c",
            "Ethel the aardvark goes quantity surveying",
            "A fun-filled adventure as our hero tackles the the boundless excitement of cost control and production schedules",
            "Mike",
            "2019-10-31T11:22:33",
            "2019-11-31T11:22:33",
            ["python"].toSet())

}
