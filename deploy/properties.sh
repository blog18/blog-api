#!/bin/bash

file="$(pwd)/cloud.properties"
bash --version

if [ -f "$file" ]
then
  echo "$file found."

  while IFS='=' read -r k v; do
    eval ${k}=\${v}
  done < "$file"

  ACCOUNT_ID=${aws_account_id}
  ACCOUNT_REGION=${aws_account_region}
  ACCOUNT_DOMAIN=${aws_account_domain}
  DOMAIN_HOSTED_ZONE_ID=${aws_account_domain_hosted_zone_id}
  DOMAIN_CERT=${aws_account_domain_certificate_arn}
  echo "ACCOUNT_ID: ${ACCOUNT_ID}"
  echo "ACCOUNT_REGION: ${ACCOUNT_REGION}"
  echo "ACCOUNT_DOMAIN: ${ACCOUNT_DOMAIN}"
  echo "DOMAIN_HOSTED_ZONE_ID: ${DOMAIN_HOSTED_ZONE_ID}"
  echo "DOMAIN_CERT: ${DOMAIN_CERT}"

  S3_AWS_RELEASES_BUCKET=releases.${ACCOUNT_DOMAIN}
  echo "S3_AWS_RELEASES_BUCKET: ${S3_AWS_RELEASES_BUCKET}"

else
  echo "$file not found: need to set an aws.properties file in the root of this project"
fi
