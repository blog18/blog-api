package com.zenithwebfoundry.blog.di.data;

import com.google.inject.Inject;
import com.zenithwebfoundry.blog.model.Article;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;

import java.util.Map;

import static com.zenithwebfoundry.blog.di.App.ARTICLE_TABLE;
import static com.zenithwebfoundry.blog.di.data.DataModule.IS_READ_CONSISTENT;
import static java.util.Collections.singletonMap;

public interface GetArticleService {

    Article get(String id);

    class Default implements GetArticleService {

        @Inject
        private DynamoDbClient dynamoDbClient;

        @Override
        public Article get(String id) {
            Map<String, AttributeValue> keys =
                    singletonMap("id", AttributeValue.builder().s(id)
                    .build());

            Article article = null;
            try {
                GetItemResponse res = dynamoDbClient.getItem(GetItemRequest.builder()
                        .key(keys)
                        .tableName(ARTICLE_TABLE)
                        .consistentRead(IS_READ_CONSISTENT)
                        .build());

                article = new Article(res.item());
            } catch (AwsServiceException | SdkClientException e) {
                e.printStackTrace();
            }

            return article;
        }
    }
}
