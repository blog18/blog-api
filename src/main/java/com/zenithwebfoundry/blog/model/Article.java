package com.zenithwebfoundry.blog.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Article extends DynamoMappable {

    private String id;
    private String title;
    private String body;
    private String author;
    private String createdAt;
    private String updatedAt;
    private Set<String> tags;

    public Article(Map<String, AttributeValue> attributeValueMap) {
        this.id = getValueIfNotNull(attributeValueMap, "id");
        this.title = getValueIfNotNull(attributeValueMap, "title");
        this.body = getValueIfNotNull(attributeValueMap, "body");
        this.author = getValueIfNotNull(attributeValueMap, "author");
        this.createdAt = getValueIfNotNull(attributeValueMap, "createdAt");
        this.updatedAt = getValueIfNotNull(attributeValueMap, "updatedAt");
        this.tags = buildModelSetIfListEntryPresent(attributeValueMap, "tags", String::valueOf);
    }

    @Override
    public Map<String, AttributeValue> toAttributeValues() {
        Map<String, AttributeValue> map = new HashMap<>();

        safelyAddString(map, "id", id);
        safelyAddString(map, "title", title);
        safelyAddString(map, "body", body);
        safelyAddString(map, "author", author);
        safelyAddString(map, "createdAt", createdAt);
        safelyAddString(map, "updatedAt", updatedAt);
        map.put("tags",
                AttributeValue.builder().l(
                        tags
                                .stream()
                                .map(t -> AttributeValue.builder().s(t).build())
                                .collect(Collectors.toList()))
                        .build());
        return map;
    }

    public static Map<String, String> asExpressionMap() {
        return getExpressionNameMap(Article.class);
    }
}
