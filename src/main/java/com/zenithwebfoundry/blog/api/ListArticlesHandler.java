package com.zenithwebfoundry.blog.api;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.zenithwebfoundry.blog.di.data.ListArticlesService;
import com.zenithwebfoundry.blog.exception.ApiException;
import com.zenithwebfoundry.blog.model.Article;
import software.amazon.awssdk.utils.IoUtils;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.stream.Collectors;

import static com.zenithwebfoundry.blog.tools.LogFactory.LOGGER;

/**
 * <p>
 *     Implementing RequestStreamHandler so as to get access to the path Parameters.
 * </p>
 * <p>
 *     Expected parameters (after the ?) are:
 *     <ol>
 *         <li>author=[author]</li>
 *         <li>month=[month]</li>
 *         <li>year=[year]</li>
 *         <li>tag=[tag]</li>
 *     </ol>
 *     In any combination.
 * </p>
 */
public class ListArticlesHandler implements RequestStreamHandler {

    @Inject
    private ListArticlesService listArticlesService;

    @Override
    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context)
            throws IOException {

        LambdaLogger logger = context.getLogger();
        logger.log("log me something, please, anything...");
        ObjectMapper om = new ObjectMapper();
        logger.log("again, anything...");

        String event = readEvent(inputStream, logger);
        logger.log("I survived the stringing of the stream!");
        logger.log("did I get the event though?: " + event);
        // LOGGER.info(this, event);
        JsonNode node = JsonNodeFactory.instance.nullNode();
        try {
            node = om.readTree(event);
        } catch (JsonProcessingException e) {
            logger.log("unable to parse event into JSON: " + event);
        }

        JsonNode queryParams = node.get("queryStringParameters");

        List<Article> articles = null;
        if (queryParams != null) {
            JsonNode author = queryParams.get("author");
            if (author != null) {
                articles = listArticlesService.listArticlesCreatedBy(author.asText());
                logger.log(articles != null ? articles.stream().map(Article::getTitle).collect(Collectors.joining()) : "null");
                // LOGGER.info(this, "articles", articles != null ? articles.stream().map(Article::getTitle).collect(Collectors.joining()) : "null");
            }

            JsonNode month = queryParams.get("month");
            JsonNode year = queryParams.get("year");
            if (month != null && year != null) {
                LocalDateTime ldts = LocalDate.of(year.asInt(), month.asInt(), 1).atStartOfDay();
                long millisStart = ldts.toEpochSecond(ZoneOffset.UTC);

                LocalDateTime ldte = LocalDate.of(year.asInt(), month.asInt(), 1).with(TemporalAdjusters.lastDayOfMonth()).atTime(23, 59, 59);
                long millisEnd = ldte.toEpochSecond(ZoneOffset.UTC);

                articles = listArticlesService.listArticlesCreatedBetween(millisStart, millisEnd);
                logger.log(articles != null ? articles.stream().map(Article::getTitle).collect(Collectors.joining()) : "null");
                //LOGGER.info(this, "articles", articles != null ? articles.stream().map(Article::getTitle).collect(Collectors.joining()) : "null");
            }

            JsonNode tag = queryParams.get("tag");
            if (tag != null) {
                articles = listArticlesService.listArticlesWithTag(tag.asText());
                logger.log(articles != null ? articles.stream().map(Article::getTitle).collect(Collectors.joining()) : "null");
                //LOGGER.info(this, "articles", articles != null ? articles.stream().map(Article::getTitle).collect(Collectors.joining()) : "null");
            }

        } else {
            // we're listing all articles
            articles = listArticlesService.listArticles();
            logger.log(articles != null ? articles.stream().map(Article::getTitle).collect(Collectors.joining()) : "null");
        }

        OutputStreamWriter writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);

        if (articles != null) {
            StreamResponse<List<Article>> res = new StreamResponse<>(200, 0, articles);
            logger.log(articles.stream().map(Article::getTitle).collect(Collectors.joining()));
            //LOGGER.info(this, "returning articles", articles.stream().map(Article::getTitle).collect(Collectors.joining()));
            String responseJson = om.writeValueAsString(res);
            writer.write(responseJson);
        } else {
            StreamResponse<String> res = new StreamResponse<>(200, 0, "none");
            logger.log("returning nothing");
            String responseJson = om.writeValueAsString(res);
            writer.write(responseJson);
        }
        writer.close();
    }


    protected String readEvent(InputStream inputStream, LambdaLogger logger) {

        logger.log("running readEvent on inputStream");
        StringBuilder textBuilder = new StringBuilder();
        Reader reader = new BufferedReader(new InputStreamReader
                (inputStream, Charset.forName(StandardCharsets.UTF_8.name())));
        int c = 0;
        try {
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
        } catch (IOException e) {
            logger.log(e.getMessage());
        }

        logger.log("returning event: " + textBuilder.toString());
        return textBuilder.toString();
    }

}
