package com.zenithwebfoundry.blog.di.asset;

import com.google.inject.AbstractModule;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.core.client.config.ClientOverrideConfiguration;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;
import software.amazon.awssdk.services.s3.S3Client;

import static com.zenithwebfoundry.blog.di.App.REGION;

public class AssetModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(S3Client.class).toInstance(s3ClientInstance);
    }

    private S3Client s3ClientInstance = make();

    private S3Client make() {
        return S3Client.builder()
                .region(REGION)
                .httpClient(UrlConnectionHttpClient.builder().build())
                .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                .overrideConfiguration(ClientOverrideConfiguration.builder().build())
                .build();
    }

}
