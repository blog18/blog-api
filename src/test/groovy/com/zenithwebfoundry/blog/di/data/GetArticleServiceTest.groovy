package com.zenithwebfoundry.blog.di.data

import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Inject
import com.zenithwebfoundry.blog.model.Article
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse
import spock.lang.Specification

class GetArticleServiceTest extends Specification {
    DynamoDbClient dataClient = Mock()

    @Inject GetArticleService getArticleService

    def setup() {
        Guice.createInjector(getTestModule()).injectMembers(this)
    }

    AbstractModule getTestModule() {
        return new AbstractModule() {
            @Override protected void configure() {
                bind(DynamoDbClient.class).toInstance(dataClient);
                bind(GetArticleService.class).to(GetArticleService.Default.class)
            }
        }
    }

    def ID = "10343816-c884-4c79-9ab4-6dd4c740c52c"
    def TITLE = "Ethel the aardvark goes quantity surveying"
    def BODY = "A fun-filled adventure as our hero tackles the the boundless excitement of cost control and production schedules"
    def AUTHOR = "Mike"
    def CREATED_AT = "2019-10-31T11:22:33"
    def UPDATED_AT = "2019-11-31T11:22:33"
    def INSTANCE = new Article(ID, TITLE, BODY, AUTHOR, CREATED_AT, UPDATED_AT, ["python"].toSet())

    def "the article is gotten"() {

        given:
            INSTANCE

            dataClient.getItem(_ as GetItemRequest) >> GetItemResponse.builder()
                    .item(INSTANCE.toAttributeValues()).build()
        when:
            Article actual = getArticleService.get(ID)

        then:
            actual.id == ID
            actual.title == TITLE
            actual.body == BODY
            actual.author == AUTHOR
            actual.createdAt == CREATED_AT
            actual.updatedAt == UPDATED_AT
    }


}
