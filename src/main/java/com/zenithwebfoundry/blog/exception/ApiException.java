package com.zenithwebfoundry.blog.exception;

public class ApiException extends RuntimeException {

    public ApiException(String errorMessage) {
        super(errorMessage);
    }

    public ApiException(String errorMessage, Throwable e) {
        super(errorMessage, e);
    }

    public ApiException(ErrorCode code) {
        super(code.toString());
    }

    public ApiException(ErrorCode code, String message) {
        super(code.toString());
    }

    public ApiException(ErrorCode code, Throwable e) {
        super(e);
    }

}