package com.zenithwebfoundry.blog.di.id;

import com.google.inject.AbstractModule;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.core.client.config.ClientOverrideConfiguration;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;

import static com.zenithwebfoundry.blog.di.App.REGION;

public class IdModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(CognitoIdentityProviderClient.class).toInstance(cognitoClientInstance);
    }

    private CognitoIdentityProviderClient cognitoClientInstance = make();

    private CognitoIdentityProviderClient make() {
        return CognitoIdentityProviderClient.builder()
                .region(REGION)
                .httpClient(UrlConnectionHttpClient.builder().build())
                .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                .overrideConfiguration(ClientOverrideConfiguration.builder().build())
                .build();
    }
}
