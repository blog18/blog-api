package com.zenithwebfoundry.blog.di.data;

import com.google.inject.AbstractModule;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.core.client.config.ClientOverrideConfiguration;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

import java.net.URI;
import java.net.URISyntaxException;

import static com.zenithwebfoundry.blog.di.App.REGION;

public class DataModule extends AbstractModule {
    public static final String DB_URL = "https://dynamodb.ap-southeast-2.amazonaws.com";
    public static final Boolean IS_READ_CONSISTENT = true;

    @Override
    protected void configure() {
        bind(DynamoDbClient.class).toInstance(dynamoDbClientInstance);
        bind(SaveArticleService.class).to(SaveArticleService.Default.class);
        bind(GetArticleService.class).to(GetArticleService.Default.class);
        bind(ListArticlesService.class).to(ListArticlesService.Default.class);
    }

    DynamoDbClient dynamoDbClientInstance = makeDynamoDbClientInstance();

    private DynamoDbClient makeDynamoDbClientInstance() {
        try {
            return DynamoDbClient.builder()
                    .region(REGION)
                    .httpClient(UrlConnectionHttpClient.builder().build())
                    .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                    .endpointOverride(new URI(DB_URL))
                    .overrideConfiguration(ClientOverrideConfiguration.builder().build())
                    .build();
        } catch (URISyntaxException e) {
            System.out.println("Malformed DB URL " + DB_URL);
            return null;
        }
    }

}
