package com.zenithwebfoundry.blog.exception;

public enum ErrorSource {

    /**
     * The none error
     */
    NONE,

    /**
     * The error came from user entry, either in an individual field or as an incompatible combination of entries.
     */
    USER,

    /**
     * The error came from an exception in our service somewhere
     */
    SYSTEM,

    /**
     * The error came from an encyption or communications issue, or from a timeout to a service or resource.
     */
    BOUNDARY
}
