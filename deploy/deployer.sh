#!/bin/bash

source $(pwd)/deploy/properties.sh

APPLICATION_VERSION=$($(pwd)/gradlew properties | sed -n 's/\(version: \)\([0-9.]*\)/\2/p')
FILE_VERSION=${APPLICATION_VERSION//./-}

echo "Creating artefact..."

$(pwd)/gradlew clean build

PROJECT_NAME=$($(pwd)/gradlew properties | sed -n 's/\(name: \)\([0-9.]*\)/\2/p')

DIST_DIR=$(pwd)/build/distributions
echo -e "Artefact Distribution Directory: $DIST_DIR"

## rename the zip file to remove the dots in the name
mv ${DIST_DIR}/${PROJECT_NAME}-${APPLICATION_VERSION}.zip ${DIST_DIR}/${PROJECT_NAME}-${FILE_VERSION}.zip

API_PACKAGE_LOCATION=${DIST_DIR}/${PROJECT_NAME}-${FILE_VERSION}.zip

echo ""
echo "Uploading API Package to Resource s3 bucket..."

echo $(aws2 --version)
echo $(aws2 s3 ls)
aws2 s3 cp $API_PACKAGE_LOCATION s3://${S3_AWS_RELEASES_BUCKET}

echo ""
echo "Creating the stack..."

echo "Checking if stack exists ..."
if ! aws2 cloudformation describe-stacks --stack-name ${PROJECT_NAME} --region ${ACCOUNT_REGION} ; then
    echo -e "\n $PROJECT_NAME Stack does not exist. Stack creation in progress, this may take a while..."
else
    echo -e "\n $PROJECT_NAME Stack exists. Updating stack, this may take few minutes..."
fi

CF_TEMPLATE_FILE=$(pwd)/deploy/blog.yml
KEY_PREFIX=${PROJECT_NAME}-${FILE_VERSION}

echo -e "Using the template: [$CF_TEMPLATE_FILE], for the project: [$PROJECT_NAME], in region: [$ACCOUNT_REGION]"
echo -e "Artefact being deployed to bucket: [$S3_AWS_RELEASES_BUCKET], under key: [$KEY_PREFIX]"
echo -e "Version being deployed: [$APPLICATION_VERSION]"

## DEPLOYMENT
## AS PART OF DEPLOYMENT S3_AWS_APP_BUCKET AND S3_AWS_ASSETS_BUCKET ARE CREATED
REQUEST_PARAM_TEMPLATE=$(cat $(pwd)/src/main/resources/templates/req_pram_mapper.vsl)

aws2 cloudformation deploy \
    --template-file "${CF_TEMPLATE_FILE}" \
    --region "${ACCOUNT_REGION}" \
  	--stack-name "${PROJECT_NAME}" \
  	--force-upload \
  	--capabilities CAPABILITY_IAM \
  	--parameter-overrides \
      ParamRequestMappingTemplate="${REQUEST_TEMPLATE}" \
  		DomainName="${ACCOUNT_DOMAIN}" \
  		DomainHostedZoneId="${DOMAIN_HOSTED_ZONE_ID}" \
  		DomainCert="${DOMAIN_CERT}" \
  		ArtefactRepositoryBucket="${S3_AWS_RELEASES_BUCKET}" \
  		ArtefactRepositoryKeyPrefix="${KEY_PREFIX}" \
  		CodeVersion="${FILE_VERSION}"
