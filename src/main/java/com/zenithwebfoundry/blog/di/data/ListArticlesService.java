package com.zenithwebfoundry.blog.di.data;

import com.google.inject.Inject;
import com.zenithwebfoundry.blog.model.Article;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.QueryRequest;
import software.amazon.awssdk.services.dynamodb.model.QueryResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.zenithwebfoundry.blog.di.App.ARTICLE_TABLE;

public interface ListArticlesService {

    List<Article> listArticlesCreatedBy(String author);

    List<Article> listArticlesCreatedBetween(Long from, Long to);

    List<Article> listArticlesWithTag(String tag);

    List<Article> listArticles();

    class Default implements ListArticlesService {

        @Inject
        private DynamoDbClient dynamoDbClient;

        public List<Article> listArticlesCreatedBy(String author) {

            Map<String,String> expressionAttributesNames = new HashMap<>();
            expressionAttributesNames.put("#author","author");

            Map<String,AttributeValue> expressionAttributeValues = new HashMap<>();
            expressionAttributeValues.put(":author", AttributeValue.builder().s(author).build());

            return performQuery(expressionAttributesNames, expressionAttributeValues);
        }

        public List<Article> listArticlesCreatedBetween(Long from, Long to) {

            Map<String,String> expressionAttributesNames = new HashMap<>();
            expressionAttributesNames.put("#createdAt","createdAt");

            Map<String,AttributeValue> expressionAttributeValues = new HashMap<>();
            expressionAttributeValues.put(":from", AttributeValue.builder().n(Long.toString(from)).build());
            expressionAttributeValues.put(":to", AttributeValue.builder().n(Long.toString(to)).build());

            return performQuery(expressionAttributesNames, expressionAttributeValues);
        }

        public List<Article> listArticles() {
            QueryRequest queryRequest = QueryRequest.builder()
                    .tableName(ARTICLE_TABLE)
                    .build();

            QueryResponse queryResponse = dynamoDbClient.query(queryRequest);

            return queryResponse.items()
                    .stream()
                    .map(Article::new)
                    .collect(Collectors.toList());
        }

        public List<Article> listArticlesWithTag(String tag) {

            Map<String,AttributeValue> expressionAttributeValues = new HashMap<>();
            expressionAttributeValues.put(":tag", AttributeValue.builder().s(tag).build());

            QueryRequest queryRequest = QueryRequest.builder()
                    .tableName(ARTICLE_TABLE)
                    .expressionAttributeValues(expressionAttributeValues)
                    .filterExpression("contains(tags, :tag)")
                    .build();

            QueryResponse queryResponse = dynamoDbClient.query(queryRequest);

            return queryResponse.items()
                    .stream()
                    .map(Article::new)
                    .collect(Collectors.toList());
        }

        private List<Article> performQuery(Map<String, String> expressionAttributesNames, Map<String, AttributeValue> expressionAttributeValues) {
            QueryRequest queryRequest = QueryRequest.builder()
                    .tableName(ARTICLE_TABLE)
                    .expressionAttributeNames(expressionAttributesNames)
                    .expressionAttributeValues(expressionAttributeValues)
                    .build();

            QueryResponse queryResponse = dynamoDbClient.query(queryRequest);

            return queryResponse.items()
                    .stream()
                    .map(Article::new)
                    .collect(Collectors.toList());

        }
    }

}
