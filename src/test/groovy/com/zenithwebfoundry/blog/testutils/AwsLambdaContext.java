package com.zenithwebfoundry.blog.testutils;

import com.amazonaws.services.lambda.runtime.ClientContext;
import com.amazonaws.services.lambda.runtime.CognitoIdentity;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;

public class AwsLambdaContext implements Context {

    RequestStreamHandler rsh;

    RequestHandler<?,?> rh;

    public AwsLambdaContext() {

    }

    public AwsLambdaContext(RequestStreamHandler h) {
        this.rsh = h;
    }

    public AwsLambdaContext(RequestHandler<?,?> h) {
        this.rh = h;
    }

    @Override
        public String getAwsRequestId() {
            return "22222222-2222-2222-2222-222222222222";
        }

        @Override
        public String getLogGroupName() {
            return null;
        }

        @Override
        public String getLogStreamName() {
            return null;
        }

        @Override
        public String getFunctionName() {
            return rsh != null ? rsh.getClass().getName() : rh.getClass().getName();
        }

        @Override
        public String getFunctionVersion() {
            return "1.0";
        }

        @Override
        public String getInvokedFunctionArn() {
            return null;
        }

        @Override
        public CognitoIdentity getIdentity() {
            return null;
        }

        @Override
        public ClientContext getClientContext() {
            return null;
        }

        @Override
        public int getRemainingTimeInMillis() {
            return 0;
        }

        @Override
        public int getMemoryLimitInMB() {
            return 0;
        }

        @Override
        public LambdaLogger getLogger() {
            return new AwsLogger();
        }

}
