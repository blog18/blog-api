package com.zenithwebfoundry.blog.di;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.zenithwebfoundry.blog.api.HandlerModule;
import com.zenithwebfoundry.blog.di.asset.AssetModule;
import com.zenithwebfoundry.blog.di.data.DataModule;
import com.zenithwebfoundry.blog.di.id.IdModule;
import software.amazon.awssdk.regions.Region;

/**
 * <p>
 *     This is essentially our main
 * </p>
 * <p>
 *     Because we are in a Lambda function context, we don't have the classic "public static void main(Sting[] args)".
 *     Instead, we simply have a static block, which contains the bootstrap code.
 * </p>
 */
public class App {
    public static final Region REGION = Region.AP_SOUTHEAST_2;
    public static final String ARTICLE_TABLE = getEnvPropertyOrSetDefault("TABLE_NAME", "ArticleTable");

    public static App APP;

    static {
        Injector injector = Guice.createInjector(new DataModule(), new IdModule(), new AssetModule(), new HandlerModule());

        APP = injector.getInstance(App.class);
    }

    /**
     * Finds the named Lambda property as is set in CloudFormation in the following block:
     * <code>
     *   SaveArticleLambda:
     *     Type: AWS::Serverless::Function
     *     DependsOn:
     *       - ArticleTable
     *     Properties:
     *       Handler: !Ref 'GetArticleHandler'
     *       Runtime: java8
     *       CodeUri:
     *         Key: !Ref 'SignInUpLambdaPackage'
     *         Bucket: !Ref 'ParamCodeBucket'
     *       MemorySize: 512
     *       Timeout: 10
     *       Tracing: Active
     *       Policies:
     *         - Version: '2012-10-17'
     *           Statement:
     *             - Effect: Allow
     *               Action:
     *                 - dynamodb:UpdateItem
     *               Resource: !GetAtt [ DynamoFormCollection, Arn ]
     *             - Effect: Allow
     *               Action:
     *                 - xray:PutTraceSegments
     *                 - xray:PutTelemetryRecords
     *               Resource: '*'
     *       Environment:
     *         Variables:
     *           TABLE_NAME: !Ref 'ArticleTable'
     * </code>
     * (see last line) or sets the specified default name.
     * @param key the env key name ("TABLE_NAME" in the above case)
     * @param defaultValue a default name specified by the caller.
     * @return the named Lambda property, or the default.
     */
    public static String getEnvPropertyOrSetDefault(String key, String defaultValue) {
        String value = System.getenv(key);
        value = (value == null) ? defaultValue : value;
        return value;
    }

}
