package com.zenithwebfoundry.blog.exception;

import lombok.Getter;

/**
 * <p>
 *     The ErrorCode is for communicating application- and network-based errors in a manner that is independent
 *     of the messaging. The ErrorCode transmits meaning, rather than explanation.
 * </p>
 * <p>
 *     The numeric codes are structured with the following meanings (the meaning may change over time):
 * </p>
 * <ul>
 *     <li>The first number represents the source of the error. </li>
 *     <ul>
 *         <li>the number 0 means no error</li>
 *         <li>the number 1 means that the error was with the user</li>
 *         <li>the number 5 means that the error was with the application</li>
 *         <li>the number 6 means that the error was with the network</li>
 *     </ul>
 *     <li>The second number represents the type of the error. </li>
 *     <ul>
 *         <li>the number 0 means generic or no particular type</li>
 *         <li>the number 1 means missing data</li>
 *         <li>the number 2 means bad formatting</li>
 *         <li>the number 3 means incorrect or inconsistent data</li>
 *     </ul>
 *     <li>The third number is usually an enumeration (one being the first error of that type. </li>
 * </ul>
 */
@Getter
public enum ErrorCode {

    NONE(0, ErrorSource.NONE),
    NULL_VALUE(11, ErrorSource.USER),
    INVALID_BOOLEAN(12, ErrorSource.USER),
    INVALID_CHARS(12, ErrorSource.USER),
    MANDATORY_NAME(11, ErrorSource.USER),
    ONE_CHAR_NAME(12, ErrorSource.USER),
    INVALID_DATE(12, ErrorSource.USER),
    VALIDATION_ERROR(10, ErrorSource.USER),
    INTERNAL_ERROR(50, ErrorSource.SYSTEM),
    INVALID_USERNAME(121, ErrorSource.USER),
    INVALID_PASSWORD(122, ErrorSource.USER),
    USER_NOT_AUTHORISED(123, ErrorSource.USER),
    INVALID_SESSION(127, ErrorSource.SYSTEM),
    ;

    private final int code;
    private final ErrorSource source;

    ErrorCode(int code, ErrorSource source) {
        this.code = code;
        this.source = source;
    }

}
