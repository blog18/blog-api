package com.zenithwebfoundry.blog.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class StreamResponse<T> {
    private int statusCode = 200;
    private int code = 0;
    private T body;
}
