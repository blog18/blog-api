package com.zenithwebfoundry.blog.api

import com.google.inject.Injector
import com.google.inject.Guice
import com.zenithwebfoundry.blog.di.data.GetArticleService
import com.zenithwebfoundry.blog.model.RequestParams
import com.zenithwebfoundry.blog.testutils.AwsLambdaContext
import com.zenithwebfoundry.blog.fixtures.ArticleFixtures
import spock.lang.Specification

class GetArticleHandlerTest extends Specification {

    GetArticleHandler handler = new GetArticleHandler()
    GetArticleService service = Mock()

    def ID = "10343816-c884-4c79-9ab4-6dd4c740c52c"
    def TITLE = "Ethel the aardvark goes quantity surveying"
    def BODY = "A fun-filled adventure as our hero tackles the the boundless excitement of cost control and production schedules"
    def AUTHOR = "Mike"
    def CREATED_AT = "2019-10-31T11:22:33"
    def UPDATED_AT = "2019-11-31T11:22:33"

    def "it handles requests properly"() {
        given:
            RequestParams req = new RequestParams();
            req.path = "99c23ada-f9b1-4f21-962b-4f1184b936d7"
            def ctx = new AwsLambdaContext(handler)
            handler.setGetArticleService(service)

        when:
            service.get("99c23ada-f9b1-4f21-962b-4f1184b936d7") >> ArticleFixtures.ETHEL_ARTICLE
            def actual = handler.handleRequest(req, ctx)

        then:
            actual.id == ID
            actual.title == TITLE
            actual.body == BODY
            actual.author == AUTHOR
            actual.createdAt == CREATED_AT
            actual.updatedAt == UPDATED_AT


    }

}
