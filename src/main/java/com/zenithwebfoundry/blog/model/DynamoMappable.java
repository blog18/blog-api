package com.zenithwebfoundry.blog.model;

import lombok.NoArgsConstructor;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.utils.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Marks a model (datatype) class as mappable to DynamoDB, and by enforcing extenders
 * to implement a constructor from {@link Map<String,AttributeValue>} and a {@link DynamoMappable#toAttributeValues}
 * serializer, we can ensure that models can be persisted to DynamoDB safely.
 */
@NoArgsConstructor
public abstract class DynamoMappable {

    /**
     * Expressly for the putpose of creating instances direcly from AWS DynamoDB API returned objects.
     *
     * @param attributeValueMap the DynamoDB model
     */
    public DynamoMappable(Map<String, AttributeValue> attributeValueMap) {

    }

    /**
     * Utility for converting the instance to a DynamoDB-persistable object
     *
     * @return the DynamoDB object representation of this instance.
     */
    public abstract Map<String, AttributeValue> toAttributeValues();

    public static <O> O buildModelIfMapEntryPresent(Map<String, AttributeValue> attributeValueMap, String key, Function<Map<String, AttributeValue>, O> constr) {
        if (attributeValueMap != null && attributeValueMap.containsKey(key)) {
            return constr.apply(getMapValueIfNotNull(attributeValueMap, key));
        }
        return null;
    }

    public static <O> Set<O> buildModelSetIfListEntryPresent(Map<String, AttributeValue> attributeValueMap, String key, Function<Map<String, AttributeValue>, O> constr) {
        if (attributeValueMap != null && attributeValueMap.containsKey(key)) {
            return attributeValueMap.get(key)
                    .l()
                    .stream()
                    .map(avm -> constr.apply(avm.m()))
                    .collect(Collectors.toSet());
        }
        return new HashSet<>();
    }


    public static String getValueIfNotNull(Map<String, AttributeValue> theMap, String k) {
        AttributeValue attributeValue = theMap.get(k);
        if (attributeValue == null) {
            return null;
        }
        return attributeValue.s();
    }

    public static Double getDoubleValueIfNotNull(Map<String, AttributeValue> theMap, String k) {
        AttributeValue attributeValue = theMap.get(k);
        if (attributeValue == null || !attributeValue.s().matches("-?\\d+(\\.\\d+)?(E-?\\d+)?")) {
            return null;
        }
        return Double.parseDouble(attributeValue.s());
    }

    public static Map<String, AttributeValue> getMapValueIfNotNull(Map<String, AttributeValue> theMap, String k) {
        AttributeValue attributeValue = theMap.get(k);
        if (attributeValue == null) {
            return null;
        }
        return attributeValue.m();
    }

    public static List<AttributeValue> getListValueIfNotNull(Map<String, AttributeValue> theMap, String k) {
        AttributeValue attributeValue = theMap.get(k);
        if (attributeValue == null) {
            return null;
        }
        return attributeValue.l();
    }

    /**
     * Utility for creating the full projection attribute set to specify the fields to get from dynamo after a Get Operation/variation thereof
     *
     * @param t Any class that extends this class
     * @return A string containing the names of all fields of the class t with a prepended '#' before every field name
     */
    protected static String getDefaultProjectionExpression(Class<? extends DynamoMappable> t) {
        return getNonStaticFieldNames(t)
                .map(n -> ("#" + n))
                .collect(Collectors.joining(","));
    }

    /**
     * Utility for creating the full expression name map to specify the fields to get from dynamo after a Get Operation/variation thereof
     * eg. if field name = "abc" , then that would map to Entry<K,V> - Entry<"#abc","abc">
     *
     * @param t Any class that extends this class
     * @return A map containing the names of all fields of the class t with a prepended '#' as the value and the name itself as the key
     */
    protected static Map<String, String> getExpressionNameMap(Class<? extends DynamoMappable> t) {
        return getNonStaticFieldNames(t)
                .collect(Collectors.toMap(n -> "#" + n, n -> n));
    }


    protected static void safelyAddString(Map<String, AttributeValue> map, String key, String value) {
        if (!StringUtils.isBlank(value)) {
            map.put(key, AttributeValue.builder().s(value).build());
        }
    }

    protected static void safelyAddDouble(Map<String, AttributeValue> map, String key, Double value) {
        if (value != null) {
            map.put(key, AttributeValue.builder().s(String.valueOf(value)).build());
        }
    }

    protected static void safelyAddMap(Map<String, AttributeValue> map, String key, Map<String, AttributeValue> value) {
        if (value != null) {
            map.put(key, AttributeValue.builder().m(value).build());
        }
    }

    protected static String getDefaultAttributesToGet(Class<? extends DynamoMappable> t) {
        return getNonStaticFieldNames(t)
                .collect(Collectors.joining(","));
    }

    private static Stream<String> getNonStaticFieldNames(Class<? extends DynamoMappable> t) {
        return Stream.of(t.getDeclaredFields())
                .filter(f -> !Modifier.isStatic(f.getModifiers()))
                .map(Field::getName);
    }

}
