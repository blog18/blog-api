package com.zenithwebfoundry.blog.di.data;

import com.google.inject.Inject;
import com.zenithwebfoundry.blog.exception.ApiException;
import com.zenithwebfoundry.blog.model.Article;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.AttributeValueUpdate;
import software.amazon.awssdk.services.dynamodb.model.DynamoDbException;
import software.amazon.awssdk.services.dynamodb.model.UpdateItemRequest;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.zenithwebfoundry.blog.di.App.ARTICLE_TABLE;
import static com.zenithwebfoundry.blog.tools.LogFactory.LOGGER;
import static java.util.Collections.singletonMap;


public interface SaveArticleService {

    Article save(Article article);

    class Default implements SaveArticleService{

        @Inject
        private DynamoDbClient dynamoDbClient;

        public Article save(Article article) {
            LOGGER.info(this, "id", article.toString());
            if (article.getId() == null) {
                article.setId(UUID.randomUUID().toString());
            }

            UpdateItemRequest updateItemRequest = UpdateItemRequest.builder()
                    .tableName(ARTICLE_TABLE)
                    .key(singletonMap("id", AttributeValue.builder().s(article.getId()).build()))
                    .attributeUpdates(article.toAttributeValues().entrySet()
                            .stream()
                            .filter(e -> !e.getKey().equals("id"))
                            .collect(Collectors.toMap(Map.Entry::getKey,
                                    entry -> AttributeValueUpdate.builder().value(entry.getValue()).build())))
                    .build();
            LOGGER.info(this, "updateItemRequest", updateItemRequest.toString());
            try {
                dynamoDbClient.updateItem(updateItemRequest);
                LOGGER.info(this, "saved or updated");
            } catch (DynamoDbException ex) {
                LOGGER.error(this, ex);
                throw new ApiException("An error occurred while trying to save your response, please wait for a few minutes and try again.");
            }

            return article;
        }
    }
}
