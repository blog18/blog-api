package com.zenithwebfoundry.blog.testutils;

import com.amazonaws.services.lambda.runtime.LambdaLogger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

public class AwsLogger implements LambdaLogger {
    @Override
    public void log(String message) {
        //
        OutputStreamWriter writer = new OutputStreamWriter(new ByteArrayOutputStream(), StandardCharsets.UTF_8);
        try {
            writer.write(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void log(byte[] message) {
        OutputStreamWriter writer = new OutputStreamWriter(new ByteArrayOutputStream(), StandardCharsets.UTF_8);
        try {
            writer.write(new String(message));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
