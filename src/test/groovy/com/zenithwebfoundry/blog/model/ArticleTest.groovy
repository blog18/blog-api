package com.zenithwebfoundry.blog.model

import com.fasterxml.jackson.databind.ObjectMapper
import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import spock.lang.Specification

class ArticleTest extends Specification {

    def ID = "10343816-c884-4c79-9ab4-6dd4c740c52c"
    def TITLE = "Ethel the aardvark goes quantity surveying"
    def BODY = "A fun-filled adventure as our hero tackles the the boundless excitement of cost control and production schedules"
    def AUTHOR = "Mike"
    def CREATED_AT = "2019-10-31T11:22:33"
    def UPDATED_AT = "2019-11-31T11:22:33"
    def INSTANCE = new Article(ID, TITLE, BODY, AUTHOR, CREATED_AT, UPDATED_AT, ["python"].toSet())
    def JSON_FORMAT = '''{"id":"10343816-c884-4c79-9ab4-6dd4c740c52c","title":"Ethel the aardvark goes quantity surveying","body":"A fun-filled adventure as our hero tackles the the boundless excitement of cost control and production schedules","author":"Mike","createdAt":"2019-10-31T11:22:33","updatedAt":"2019-11-31T11:22:33","tags":["python"]}'''

    def mapper = new ObjectMapper()

    def "the field of earth"() {
        given:
            INSTANCE
        when:
            def id = INSTANCE.id
            def title = INSTANCE.title
            def body = INSTANCE.body
            def author = INSTANCE.author
            def createdAt = INSTANCE.createdAt
            def updatedAt = INSTANCE.updatedAt

        then:
            id == ID
            title == TITLE
            body == BODY
            author == AUTHOR
            createdAt == CREATED_AT
            updatedAt == UPDATED_AT
    }

    def "earth as delivered to DynamoDB"() {
        given:
            INSTANCE
        when:
            def map = INSTANCE.toAttributeValues()

        then:
            map['id'].s() == "10343816-c884-4c79-9ab4-6dd4c740c52c"
            map['title'].s() == "Ethel the aardvark goes quantity surveying"
            map['body'].s() == "A fun-filled adventure as our hero tackles the the boundless excitement of cost control and production schedules"
            map['author'].s() == "Mike"
            map['createdAt'].s() == "2019-10-31T11:22:33"
            map['updatedAt'].s() == "2019-11-31T11:22:33"
    }

    def "earth built from an attribute value map"() {
        given:
            Map<String, AttributeValue> avm = [
                    "id":AttributeValue.builder().s("10343816-c884-4c79-9ab4-6dd4c740c52c").build(),
                    "title":AttributeValue.builder().s("Ethel the aardvark goes quantity surveying").build(),
                    "body":AttributeValue.builder().s("A fun-filled adventure as our hero tackles the the boundless excitement of cost control and production schedules").build(),
                    "author":AttributeValue.builder().s("Mike").build(),
                    "createdAt":AttributeValue.builder().s("2019-10-31T11:22:33").build(),
                    "updatedAt":AttributeValue.builder().s("2019-11-31T11:22:33").build()
            ]

        when:
            def article = new Article(avm)
        then:
            article.id == ID
            article.title == TITLE
            article.body == BODY
            article.author == AUTHOR
            article.createdAt == CREATED_AT
            article.updatedAt == UPDATED_AT
    }

    def "should serialize to JSON"() {
        given:
            INSTANCE
        when:
            def json = mapper.writeValueAsString(INSTANCE)
        then:
            json == JSON_FORMAT
    }

    def "should deserialize from JSON"() {
        given:
            JSON_FORMAT
        when:
            def article = mapper.readValue(JSON_FORMAT, Article.class)
        then:
            article.id == ID
            article.title == TITLE
            article.body == BODY
            article.author == AUTHOR
            article.createdAt == CREATED_AT
            article.updatedAt == UPDATED_AT
    }

}
