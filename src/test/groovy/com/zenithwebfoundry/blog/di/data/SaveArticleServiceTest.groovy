package com.zenithwebfoundry.blog.di.data

import com.google.inject.Guice;
import com.google.inject.AbstractModule
import com.google.inject.Inject
import com.zenithwebfoundry.blog.model.Article
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.UpdateItemRequest
import software.amazon.awssdk.services.dynamodb.model.UpdateItemResponse
import spock.lang.Specification

class SaveArticleServiceTest extends Specification {


    DynamoDbClient dataClient = Mock()

    @Inject SaveArticleService saveArticleService

    def setup() {
        Guice.createInjector(getTestModule()).injectMembers(this)
    }
    AbstractModule getTestModule() {
        return new AbstractModule() {
            @Override protected void configure() {
                bind(DynamoDbClient.class).toInstance(dataClient);
                bind(SaveArticleService.class).to(SaveArticleService.Default.class)
            }
        }
    }



    def TITLE = "Ethel the aardvark goes quantity surveying"
    def BODY = "A fun-filled adventure as our hero tackles the the boundless excitement of cost control and production schedules"
    def AUTHOR = "Mike"
    def CREATED_AT = "2019-10-31T11:22:33"
    def UPDATED_AT = "2019-11-31T11:22:33"
    def INSTANCE = new Article(null, TITLE, BODY, AUTHOR, CREATED_AT, UPDATED_AT, ["python"].toSet())

    def "the article is saved"() {

        given:
            INSTANCE
            dataClient.updateItem(_ as UpdateItemRequest) >> UpdateItemResponse.builder().build()
        when:
            Article actual = saveArticleService.save(INSTANCE)

        then:
            actual.id != null
            actual.title == TITLE
            actual.body == BODY
            actual.author == AUTHOR
            actual.createdAt == CREATED_AT
            actual.updatedAt == UPDATED_AT
    }

    def "the article is updated"() {

        given:
            INSTANCE
            INSTANCE.id = "10343816-c884-4c79-9ab4-6dd4c740c52c"
            dataClient.updateItem(_ as UpdateItemRequest) >> UpdateItemResponse.builder().build()
        when:
            Article actual = saveArticleService.save(INSTANCE)

        then:
            actual.id == "10343816-c884-4c79-9ab4-6dd4c740c52c"
            actual.title == TITLE
            actual.body == BODY
            actual.author == AUTHOR
            actual.createdAt == CREATED_AT
            actual.updatedAt == UPDATED_AT
    }

}
