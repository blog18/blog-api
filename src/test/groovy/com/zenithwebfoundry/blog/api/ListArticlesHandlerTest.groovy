package com.zenithwebfoundry.blog.api

import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Inject
import com.zenithwebfoundry.blog.di.data.ListArticlesService
import com.zenithwebfoundry.blog.fixtures.ArticleFixtures
import com.zenithwebfoundry.blog.testutils.AwsLambdaContext
import spock.lang.Specification

import java.nio.charset.StandardCharsets

class ListArticlesHandlerTest extends Specification {


    ListArticlesService service = Mock()

    @Inject ListArticlesHandler handler

    def setup() {
        Guice.createInjector(getTestModule()).injectMembers(this)
    }

    AbstractModule getTestModule() {
        return new AbstractModule() {
            @Override protected void configure() {
                bind(ListArticlesService.class).toInstance(service);
                bind(ListArticlesHandler.class).toInstance(new ListArticlesHandler());
            }
        }
    }


    def "it handles requests properly"() {
        given:
            def input = '''{"queryStringParameters":{"author":"Mike"}}'''
            def inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8))

            def output = '''{"statusCode": 200}'''
            def outPutStream = new ByteArrayOutputStream(10)
            def ctx = new AwsLambdaContext(handler)

        when:
            service.listArticlesCreatedBy("Mike") >> [ArticleFixtures.ETHEL_ARTICLE]
            handler.handleRequest(inputStream, outPutStream, ctx)

        then:
            def res = new String(outPutStream.toByteArray())
            res == '''{"statusCode":200,"code":0,"body":[{"id":"10343816-c884-4c79-9ab4-6dd4c740c52c","title":"Ethel the aardvark goes quantity surveying","body":"A fun-filled adventure as our hero tackles the the boundless excitement of cost control and production schedules","author":"Mike","createdAt":"2019-10-31T11:22:33","updatedAt":"2019-11-31T11:22:33","tags":["python"]}]}'''
    }
}
