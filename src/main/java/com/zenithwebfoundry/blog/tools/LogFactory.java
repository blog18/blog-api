package com.zenithwebfoundry.blog.tools;

import com.amazonaws.services.lambda.runtime.Context;
import com.zenithwebfoundry.blog.exception.ApiException;
import com.zenithwebfoundry.blog.exception.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.MDC;
import software.amazon.awssdk.utils.StringUtils;

/**
 * <p>
 * Provides a builder-based logging factory with wrappers for each of the logging levels.
 * </p>
 * <p>
 * Uses a Message Diagnostic Context in a monitor (via ThreadLocal) for the requesting thread.
 * </p>
 */
@Slf4j
public class LogFactory {

    public static final LogFactory LOGGER = new LogFactory();
    public static final String LOG_PARAM_SESSION_ID = "SessionID";
    public static final String LOG_PARAM_REQUEST_ID = "Amzn-Request-Id";
    public static final String LOG_PARAM_COGNITO_ID = "Amzn-Cognito-Id";
    public static final String LOG_PARAM_MODULE_NAME = "ModuleName";
    public static final String LOG_PARAM_SUBMODULE_NAME = "SubmoduleName";
    public static final String LOG_PARAM_ERR_CODE = "code";
    public static final String LOG_PARAM_MESSAGE_KEY = "message";
    public static final String EQUALS_DELIMITER = "=";
    public static final String COMMA_DELIMITER = ",";


    /**
     * A wrapper for {@link Logger#trace(String)}
     *
     * @param caller the calling instance - used to identify the class that the log message is from.
     * @param pairs  the additional logged keys, values and messages.
     */
    public String trace(Object caller, String... pairs) {
        Logger log = getLogger(caller);

        String message = buildMessage((Object[]) pairs);

        log.trace(message);

        return message;
    }

    /**
     * A wrapper for {@link Logger#warn(String)}
     *
     * @param caller the calling instance - used to identify the class that the log message is from.
     * @param pairs  the additional logged keys, values and messages.
     */
    public String warn(Object caller, String... pairs) {
        Logger log = getLogger(caller);

        String message = buildMessage((Object[]) pairs);

        log.warn(message);

        return message;
    }

    /**
     * A wrapper for {@link Logger#debug(String)}
     *
     * @param caller the calling instance - used to identify the class that the log message is from.
     * @param pairs  the additional logged keys, values and messages.
     */
    public String debug(Object caller, String... pairs) {
        Logger log = getLogger(caller);

        String message = buildMessage((Object[]) pairs);

        log.debug(message);

        return message;
    }

    /**
     * A wrapper for {@link Logger#info(String)}
     *
     * @param caller the calling instance - used to identify the class that the log message is from.
     * @param pairs  the additional logged keys, values and messages.
     */
    public String info(Object caller, String... pairs) {
        Logger log = getLogger(caller);

        String message = buildMessage((Object[]) pairs);

        log.info(message);

        return message;
    }

    /**
     * Logs an error without throwing the supplied {@link Exception}, if there is a message, the message is
     * presented in the log, otherwise the exception's class name is.
     *
     * @param caller the calling instance - used to identify the class that the log message is from.
     * @param e      the Exception that we are swallowing for this message
     */
    public String error(Object caller, Exception e) {
        Logger log = getLogger(caller);

        String errorMessage;
        if (StringUtils.isNotBlank(e.getLocalizedMessage())) {
            errorMessage = e.getLocalizedMessage();
        } else {
            errorMessage = e.getClass().getSimpleName();
        }

        String message = buildMessage(errorMessage);

        log.error(message, e);

        return message;
    }

    /**
     * Logs an error without throwing the supplied {@link Exception}, if there is a message, the message is
     * presented in the log, otherwise the exception's class name is.
     *
     * @param caller the calling instance - used to identify the class that the log message is from.
     * @param e      the Exception that we are swallowing for this message
     * @param code   the {@link ErrorCode}.
     */
    public String error(Object caller, Exception e, ErrorCode code) {
        Logger log = getLogger(caller);

        String message = buildMessage(LOG_PARAM_ERR_CODE, code.toString());

        log.error(message, e);

        return message;
    }

    /**
     * Logs an error without throwing the supplied {@link Exception}, if there is a message, the message is
     * presented in the log, otherwise the exception's class name is.
     *
     * @param caller the calling instance - used to identify the class that the log message is from.
     * @param e      the Exception that we are swallowing for this message
     * @param code   the {@link ErrorCode}.
     * @param pairs  the additional logged keys, values and messages.
     */
    public String error(Object caller, Exception e, ErrorCode code, String... pairs) {
        Logger log = getLogger(caller);

        String message = buildMessage(LOG_PARAM_ERR_CODE, code.toString(), pairs);

        log.error(message, e);

        return message;
    }

    /**
     * Logs an error and throws an {@link ApiException}
     *
     * @param caller the calling instance - used to identify the class that the log message is from.
     * @param pairs  the additional logged keys, values and messages.
     */
    public boolean errorAndThrow(Object caller, String... pairs) {
        Logger log = getLogger(caller);

        String message = buildMessage((Object[]) pairs);
        log.error(message);
        throw new ApiException(message);
    }

    /**
     * Logs an error and throws an {@link ApiException}
     *
     * @param caller     the calling instance - used to identify the class that the log message is from.
     * @param code       the {@link ErrorCode} to be logged.
     * @param moduleName an identifier of the business process that was running when this error was logged and thrown.
     *                   in most cases, this will represent the QuestionModule or Question being answered, but it
     *                   could represent some other process.
     */
    public void errorAndThrow(Object caller, ErrorCode code, String moduleName) {
        Logger log = getLogger(caller);

        String message = buildMessage(LOG_PARAM_ERR_CODE, code.toString(), LOG_PARAM_MODULE_NAME, moduleName);
        log.error(message);
        throw new ApiException(message);
    }

    /**
     * Logs an error and throws an {@link ApiException}
     *
     * @param caller the calling instance - used to identify the class that the log message is from.
     * @param code   the {@link ErrorCode} to be logged.
     * @param pairs  the additional logged keys, values and messages.
     */
    public void errorAndThrow(Object caller, ErrorCode code, String... pairs) {
        Logger log = getLogger(caller);
        String message = buildMessageWithErrorCode(code, pairs);
        log.error(message);
        throw new ApiException(message);
    }

    /**
     * Logs an error and throws an {@link ApiException}
     *
     * @param caller the calling instance - used to identify the class that the log message is from.
     * @param code   the {@link ErrorCode} to be logged.
     * @param ex     the Exception that we are logging, then wrapping and throwing for this message
     * @param pairs  the additional logged keys, values and messages.
     */
    public void errorAndThrow(Object caller, ErrorCode code, Exception ex, String... pairs) {
        Logger log = getLogger(caller);
        String message = buildMessageWithErrorCode(code, pairs);
        log.error(message, ex);
        throw new ApiException(message);
    }

    /**
     * Logs an error and throws an {@link ApiException} wrapping an inner exception
     *
     * @param caller the calling instance - used to identify the class that the log message is from.
     * @param ex     the Exception that we are logging, then wrapping and throwing for this message
     * @param pairs  the additional logged keys, values and messages.
     */
    public void errorAndThrow(Object caller, Exception ex, String... pairs) {
        Logger log = getLogger(caller);

        String[] newArray = new String[pairs.length + 2];
        System.arraycopy(pairs, 0, newArray, 0, pairs.length);
        newArray[pairs.length] = "message";
        newArray[pairs.length + 1] = ex.getMessage();
        String message = buildMessage((Object[]) newArray);
        log.error(message, ex);
        throw new ApiException(message);
    }


    /**
     * <p>
     * Called by a Lambda to set up the thread's Message Diagnostic Context
     * </p>
     * <p>
     * Initialises the LOGGER with the AWS Lambda {@link Context}, and allows the caller to establish a business
     * module name.
     * </p>
     *
     * @param context    the Lambda's calling/execution context
     * @param moduleName the name of the business process being fulfilled by the initiating Lambda.
     */
    public void init(Context context, String moduleName, String submoduleName) {

        MDC.put(LOG_PARAM_REQUEST_ID, context.getAwsRequestId());
        MDC.put(LOG_PARAM_MODULE_NAME, moduleName);
        MDC.put(LOG_PARAM_SUBMODULE_NAME, submoduleName);

    }

    public void destroy() {
        MDC.clear();
    }


    protected String buildMessage(Object... pairs) {

        StringBuilder bob = new StringBuilder();

        bob
                .append(LOG_PARAM_REQUEST_ID).append(EQUALS_DELIMITER).append(MDC.get(LOG_PARAM_REQUEST_ID))
                .append(COMMA_DELIMITER);

        if (MDC.get(LOG_PARAM_COGNITO_ID) != null) {
            bob.append(COMMA_DELIMITER)
                    .append(LOG_PARAM_COGNITO_ID).append(EQUALS_DELIMITER).append(MDC.get(LOG_PARAM_COGNITO_ID));
        }
        if (MDC.get(LOG_PARAM_SESSION_ID) != null) {
            bob.append(COMMA_DELIMITER)
                    .append(LOG_PARAM_SESSION_ID).append(EQUALS_DELIMITER).append(MDC.get(LOG_PARAM_SESSION_ID));
        }
        if (MDC.get(LOG_PARAM_MODULE_NAME) != null) {
            bob.append(COMMA_DELIMITER)
                    .append(LOG_PARAM_MODULE_NAME).append(EQUALS_DELIMITER).append(MDC.get(LOG_PARAM_MODULE_NAME));
        }
        if (MDC.get(LOG_PARAM_SUBMODULE_NAME) != null) {
            bob.append(COMMA_DELIMITER)
                    .append(LOG_PARAM_SUBMODULE_NAME).append(EQUALS_DELIMITER).append(MDC.get(LOG_PARAM_SUBMODULE_NAME));
        }

        if (pairs != null && pairs.length > 0) {

            for (int i = 0; i < pairs.length; i = i + 2) {
                if (pairs.length == i + 1) {
                    bob.append(COMMA_DELIMITER).append(LOG_PARAM_ERR_CODE).append(EQUALS_DELIMITER).append(pairs[i]);
                } else {
                    if (pairs[i].equals(LOG_PARAM_MODULE_NAME) && (MDC.get(LOG_PARAM_MODULE_NAME) == null)) {
                        MDC.put(LOG_PARAM_MODULE_NAME, (String) pairs[i + 1]);
                    } else if (pairs[i].equals(LOG_PARAM_SUBMODULE_NAME) && (MDC.get(LOG_PARAM_SUBMODULE_NAME) == null)) {
                        MDC.put(LOG_PARAM_SUBMODULE_NAME, (String) pairs[i + 1]);
                    } else {
                        bob.append(COMMA_DELIMITER).append(pairs[i]).append(EQUALS_DELIMITER).append(pairs[i + 1]);
                    }
                }
            }
        }
        return bob.toString();
    }

    private String buildMessageWithErrorCode(ErrorCode code, String... pairs) {
        String[] newArray = new String[pairs.length + 2];
        System.arraycopy(pairs, 0, newArray, 0, pairs.length);
        newArray[pairs.length] = LOG_PARAM_ERR_CODE;
        newArray[pairs.length + 1] = code.toString();
        return buildMessage((Object[]) newArray);
    }

    private static Logger getLogger(Object caller) {
        Class callerClass = caller.getClass();
        if (callerClass.equals(String.class) || callerClass.isInstance(Exception.class)) {
            callerClass = LogFactory.class;
        }
        return org.slf4j.LoggerFactory.getLogger(callerClass);
    }

}
