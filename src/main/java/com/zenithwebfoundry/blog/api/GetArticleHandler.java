package com.zenithwebfoundry.blog.api;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.zenithwebfoundry.blog.di.data.GetArticleService;
import com.zenithwebfoundry.blog.model.Article;
import com.zenithwebfoundry.blog.model.RequestParams;

import static com.zenithwebfoundry.blog.tools.LogFactory.LOGGER;


public class GetArticleHandler implements RequestHandler<RequestParams, Article> {

    Injector injector = Guice.createInjector(new GetArticleHandlerModule());

    private GetArticleService getArticleService = injector.getInstance(GetArticleService.class);

    public void setGetArticleService(GetArticleService getArticleService) {
        this.getArticleService = getArticleService;
    }

    @Override
    public Article handleRequest(RequestParams params, Context context) {
        LOGGER.init(context, "GetArticle", null);

        LOGGER.info(this, params.getPath());

        Article article = getArticleService.get(params.getPath());

        return article;
    }

}
