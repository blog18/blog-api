package com.zenithwebfoundry.blog.api;

import com.google.inject.AbstractModule;

public class HandlerModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(GetArticleHandler.class).toInstance(new GetArticleHandler());
        bind(ListArticlesHandler.class).toInstance(new ListArticlesHandler());
        bind(SaveArticleHandler.class).toInstance(new SaveArticleHandler());
    }

}
