package com.zenithwebfoundry.blog.api;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.zenithwebfoundry.blog.di.data.SaveArticleService;
import com.zenithwebfoundry.blog.model.Article;

import javax.inject.Inject;

import static com.zenithwebfoundry.blog.tools.LogFactory.LOGGER;

public class SaveArticleHandler implements RequestHandler<Article, Article> {

    @Inject
    private SaveArticleService saveArticleService;

    @Override
    public Article handleRequest(Article article, Context context) {
        LOGGER.init(context, "SaveArticle", null);

        return saveArticleService.save(article);
    }
}
