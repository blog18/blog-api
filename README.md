Blog API
========

A Java 11 implementation for provisioning AWS Api Gateway, A Series of CRUD Lambdas & DynamoDb for the Blog API.



## Building and Deploying

The project uses a shell script to build the artefact and drop it into `build/distributions` before performing a 
AWS CloudFormation deploy into your nominated AWS Account.

#### AWS Account
To use this project, you will need:
1. an AWS Account;
1. a registered domain name;
1. a certificate registered in `us-east-1` for all subdomains of that domain, i.e., `*.your.domain.name`;
1. the `aws cli` installed; and
1. a file at the path `~/.aws/credentials`.

With the last item, you will need to have the following content as a minimum:
```
[default]
aws_access_key_id=AKIAXXXXXXXXXX3KLTJR
aws_secret_access_key=kWwqdwe9c99fFUxxxxxxxxxxxxxxxxxxxxlllRuW
```
obviously with the real `aws_access_key_id` and `aws_secret_access_key`.

#### AWS Properties File

Add a file called `cloud.properties` to the root of this project, with the following content:

```properties
aws_account_id=000000000000
aws_account_region=xx-xxxxxxxxx-x
aws_account_domain=xxx.com
aws_account_domain_hosted_zone_id=X1X1X1X1X1X1X1
aws_account_domain_certificate_arn=arn:aws:acm:us-east-1:000000000000:certificate/00000000-0000-0000-0000-000000000000
```
putting your own details for the AWS account 12-digit ID, the region your account is in, and the domain name you have registered.
You will also need to find and specify the HostedZoneId that AWS gave for this domain, and you will need to put the arn of the certificate mentioned earlier.

#### Run the Build & Deployment
This is a Gradle build and AWS CloudFormation stack deployment, using a shell script. From the command line within the 
project root directory, run:
```
deploy/deployer.sh
```
There is quite a lot of command line output, check that you have values for everything

#### Test the Build & Deployment
```
curl -i -H "Accept: application/json" -X GET https://blogapi.<your.domain.name>/article
```
You can also test the API Gateway resource directly:
```
curl -i -H "Accept: application/json" -X GET https://<apigateway.id>.execute-api.<aws.region>.amazonaws.com/prod/articles
```

#### Sorry, Windows Users
I don't have much idea on how to write batch files, and I don't have a Windows machine to test them on, anyway. So to
get this project working from a Windows machine, you would need to write your own batch file to accomplish what
`properties.sh` and `deployer.sh` do.

#### Sorry, GCP and Azure Customers
This project assumes that you have an AWS account, to make it work with GCP or Azure, you would need to:
1. heavily modify or replace `blog.yml`, `properties.sh` and `deployer.sh`;
1. probably rebuild the `*Handlers` in `com.zenithwebfoundry.api` to suit whatever compute functions these other
providers offer; and
1. Potentially change `build.gradle` to repackage the artefact.
